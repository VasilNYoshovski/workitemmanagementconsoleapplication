# Work Item Management (WIM) Console Application

---

# Created by Vasil Yoshovski student at Telerik Academy's Alpha .NET Jan 2019

---

### Vasil Nedkov Yoshovski: fb_10212583742531654

---

## Design details:
### Composite Design Pattern 
### Autofac - an addictive Inversion of Control container for .NET Core

---

---

## Links:
[Class diagram of types](https://gitlab.com/VasilNYoshovski/workitemmanagementconsoleapplication/blob/master/docs/WorkItemClassDiagram.png)
