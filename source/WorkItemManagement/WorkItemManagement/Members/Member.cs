﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkItemManagement.Histories;
using WorkItemManagement.WorkItems;
using WorkItemManagement.WorkItems.Abstract;

namespace WorkItemManagement.Members
{
    public class Member : IMember
    {
        //Владо
        private string name;
        private readonly IDictionary<string, IWorkItem> workItemList;
        private readonly IList<IActivityHistory> history;

        public Member(string memberName, IDictionary<string, IWorkItem> workItemList, IList<IActivityHistory> history)
        {
            this.workItemList = new Dictionary<string, IWorkItem>(workItemList);
            this.history = new List<IActivityHistory>(history);
            this.Name = memberName;
        }

        public Member(string name, string activityLocation)
        {
            this.history = new List<IActivityHistory>();
            this.workItemList = new Dictionary<string, IWorkItem>();
            this.Name = name;
            this.AddActivityHistory(new ActivityHistory($"Member with name {this.Name} was added to {activityLocation}."));
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("The member's name cannot be empty!");
                }
                else if (value.Length < 5 || value.Length > 15)
                {
                    throw new ArgumentException("Member's name length should be between 5 and 15 symbols.");
                }
                else
                {
                    this.name = value;
                }
            }
        }
        public IDictionary<string, IWorkItem> WorkItemList
        {
            get
            {
                return new Dictionary<string, IWorkItem>(this.workItemList);
            }

            //set
            //{
            //    if (value != null)
            //    {
            //        foreach (var item in value)
            //        {
            //            this.workItemList.Add(item);
            //        }
            //    }
            //    else
            //    {
            //        throw new ArgumentException("A Work Item cannot be nonexistent!");
            //    }
            //}

        }
        public IList<IActivityHistory> History
        {
            get
            {
                return (IList<IActivityHistory>)this.history.AsEnumerable<IActivityHistory>();
            }
            set
            {
                if (value != null)
                {
                    foreach (var record in value)
                    {
                        this.history.Add(record);
                    }
                }
                else
                {
                    throw new ArgumentException("A record cannot be nonexistent!");
                }
            }
        }

        public void AddWorkItem(IWorkItem wi)
        {
            if (null != wi)
            {
                if (false == IsExistingWorkItem(wi.Id))
                {
                    this.workItemList.Add(wi.Id, wi);
                    AddActivityHistory(new ActivityHistory($"{wi.GetType().Name} {wi.Id} is added to {this.Name} member"));
                }
                else
                {
                    throw new ArgumentNullException($"{wi.GetType().Name} already exists in the dictionary");
                }
            }
            else
            {
                throw new ArgumentNullException("WorkItem is null");
            }
        }

        public void AddActivityHistory(IActivityHistory record)
        {
            if (record != null)
            {
                this.history.Add(record);
            }
            else
            {
                throw new ArgumentNullException("Record is null");
            }
        }

        public StringBuilder GetAllActivitiesOfMember()
        {
            
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"All ActivityHistories of member {this.name}:");
            
            foreach (var item in this.history)
            {
                sb.AppendLine(item.GetActivityData().ToString());
            }
            return sb;
        }


        public StringBuilder GetAllWorkItemsOfMember()
        {
            const string newLineString = "\r\n";
            StringBuilder sb = new StringBuilder();
            sb.Append($"All WorkItems of board {this.name} reports need to be done");
            sb.Append(newLineString);

            return sb;
        }

        public bool IsExistingWorkItem(string wiID)
        {
            foreach (var item in this.workItemList)
            {
                if (wiID == item.Key)
                {
                    return true;
                }
            }
            return false;
        }
    }
}

