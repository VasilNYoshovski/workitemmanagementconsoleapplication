﻿using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Histories;
using WorkItemManagement.WorkItems.Abstract;

namespace WorkItemManagement.Members
{
    public interface IMember
    {
        string Name { get; }
        IDictionary<string, IWorkItem> WorkItemList { get; }
        IList<IActivityHistory> History { get; }

        bool IsExistingWorkItem(string wiID);
        void AddWorkItem(IWorkItem wi);
        void AddActivityHistory(IActivityHistory ah);

        StringBuilder GetAllActivitiesOfMember();
        StringBuilder GetAllWorkItemsOfMember();
    }
}
