﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Boards;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Histories;
using WorkItemManagement.Members;
using WorkItemManagement.WorkItems;
using WorkItemManagement.WorkItems.Abstract;

namespace WorkItemManagement.Teams
{
    public class Team : ITeam
    {
        //Руси
        #region Fields
        private string name;
        private readonly IDictionary<string, IMember> membersDictionary;
        private readonly IDictionary<string, IBoard> boardsDictionary;
        private readonly IList<IActivityHistory> historyOfTeam;

        #endregion
        #region Constructors

        public Team(string name)
        {
            this.Name = name;
            this.membersDictionary = new Dictionary<string, IMember>();
            this.boardsDictionary = new Dictionary<string, IBoard>();
            this.historyOfTeam = new List<IActivityHistory>();
            historyOfTeam.Add(new ActivityHistory($"Team with name {this.Name} was successfully created."));
           
        }
        #endregion
        #region Properties
        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentNullException("The name connot be emptry");
                }
                this.name = value;
            }
        }

        public IDictionary<string, IMember> MembersDictionary
        {
            get
            {
                return new Dictionary<string, IMember>(this.membersDictionary);
            }
        }

        public IDictionary<string, IBoard> BoardsDictionary
        {
            get
            {
                return new Dictionary<string, IBoard>(this.boardsDictionary);
            }
        }
        public IList<IActivityHistory> HistoryOfTeam
        {
            get
            {
                return new List<IActivityHistory>(this.historyOfTeam);
            }
        }
        #endregion
        #region Methods
        public void AddMemberToTeam(IMember member)
        {
            if (member == null)
            {
                throw new ArgumentNullException("A member cannot be empty.");
            }
            if (IsExistingTeamMember(member.Name))
            {
                throw new ArgumentException("The member already exists.");
            }
            else
            {
                this.membersDictionary.Add(member.Name, member);
                var history = new ActivityHistory(string.Format("Member with name {0} was added to team {1}.", member.Name, this.Name));
                this.historyOfTeam.Add(history);
            }
            //this.membersDictionary.Add(member.Name, member);
        }
        public void AddBoardToTeam(IBoard board)
        {
            if (board == null)
            {
                throw new ArgumentNullException("A Board cannot be empty.");
            }
            if (IsExistingTeamBoard(board.Name))
            {
                throw new ArgumentException("The board already exists.");
            }
            else
            {
                this.boardsDictionary.Add(board.Name, board);
                var history = new ActivityHistory(string.Format("Board with name {0} was added to team {1}.", board.Name, this.Name));
                this.historyOfTeam.Add(history);
            }
        }
        public StringBuilder GetAllMembers()
        {
            var sb = new StringBuilder();
            foreach (var member in this.membersDictionary)
            {
                sb.AppendLine(member.Key);
            }
            return sb;
        }
        public StringBuilder GetAllBoards()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"All boards of team {this.name} are:");
            foreach (var board in this.boardsDictionary)
            {
                sb.AppendLine(board.Key);
            }
            return sb;
        }
        public bool IsExistingTeamMember(string memberName)
        {
            foreach (var item in this.membersDictionary)
            {
                if (memberName == item.Key)
                {
                    return true;
                }
            }
            return false;
        }
        public bool IsExistingMemberWorkItem(string memberName, string wiID)
        {
            if(false == IsExistingTeamMember(memberName))
            {
                throw new ArgumentException("Member does not exist.");
            }
            return this.membersDictionary[memberName].IsExistingWorkItem(wiID);
        }
        public void AddWorkItemToMember(string memberName, IWorkItem wi)
        {
            if (wi == null)
            {
                throw new ArgumentNullException("A WorkItem cannot be empty.");
            }
            if (true == IsExistingMemberWorkItem(memberName, wi.Id))
            {
                throw new ArgumentException("The WorkItem is already assigned to the member.");
            }
            else
            {
                this.membersDictionary[memberName].AddWorkItem(wi);
            }
        }
        public void AddHistoryToTeamActivityHistory(IActivityHistory historia)
        {
            if (historia == null)
            {
                throw new ArgumentNullException("A hystory must contain description.");
            }
            else
            {
                this.historyOfTeam.Add(historia);
            }
        }
        public bool IsExistingTeamBoard(string boardName)
        {
            foreach (var item in this.boardsDictionary)
            {
                if (boardName == item.Key)
                {
                    return true;
                }
            }
            return false;
        }
        public bool IsExistingBoardWorkItem(string boardName, string wiID)
        {
            if (false == IsExistingTeamBoard(boardName))
            {
                throw new ArgumentException("Board does not exist.");
            }
            return this.boardsDictionary[boardName].IsExistingWorkItem(wiID);
        }
        public void AddWorkItemToBoard(string boardName, IWorkItem wi)
        {
            if (wi == null)
            {
                throw new ArgumentNullException("A WorkItem cannot be empty.");
            }
            if (true == IsExistingBoardWorkItem(boardName, wi.Id))
            {
                throw new ArgumentException("The WorkItem is already assigned to the board.");
            }
            else
            {
                this.boardsDictionary[boardName].AddWorkItem(wi);
            }
        }
        public StringBuilder GetAllHistoryData()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"The full history data of team {this.name} is:");
            foreach (var history in this.HistoryOfTeam)
            {
                sb.AppendLine($"On {history.ActivityDate}: {history.ActivityRecords[0]}." );
            }
            return sb;
        }
        //public void PrintHistoryData()
        //{
        //    Console.WriteLine(GetAllHistoryData().ToString());
        //}
        #endregion
    }
}
