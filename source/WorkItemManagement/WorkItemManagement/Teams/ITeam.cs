﻿using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Boards;
using WorkItemManagement.Histories;
using WorkItemManagement.Members;
using WorkItemManagement.WorkItems.Abstract;

namespace WorkItemManagement.Teams
{
    public interface ITeam
    {
        string Name { get; }
        IDictionary<string, IMember> MembersDictionary { get; }
        IDictionary<string, IBoard> BoardsDictionary { get; }
        IList<IActivityHistory> HistoryOfTeam { get; }

        bool IsExistingTeamMember(string memberName);
        void AddMemberToTeam(IMember member);
        bool IsExistingMemberWorkItem(string memberName, string wiID);
        void AddWorkItemToMember(string memberName, IWorkItem wi);
        bool IsExistingTeamBoard(string boardName);
        void AddBoardToTeam(IBoard board);
        bool IsExistingBoardWorkItem(string boardName, string wiID);
        void AddWorkItemToBoard(string boardName, IWorkItem wi);
        void AddHistoryToTeamActivityHistory(IActivityHistory historia);

        StringBuilder GetAllMembers();
        StringBuilder GetAllBoards();
    }
}
