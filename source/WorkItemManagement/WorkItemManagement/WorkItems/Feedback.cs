﻿using System;
using System.Text;
using WorkItemManagement.Histories;
using WorkItemManagement.WorkItems.Enums;

namespace WorkItemManagement.WorkItems
{
    public class Feedback : WorkItem, IFeedback
    {
        //Владо
        private int rating;
        private StatusFeedback status;

        public Feedback(string title,string description,StatusFeedback status, int rating, string comment)
            : base(title, description, comment)
        {
            this.Rating = rating;
            this.Status = status;
            addHistory(new ActivityHistory($"{this.GetType().Name} with title" + title.ToString() + " is created!"));
        }

        public int Rating
        {
            get
            {
                return this.rating;
            }
            set
            {
                if (value.GetType() != typeof(int))
                {
                    throw new ArgumentException("The rating must be an integer!");
                }
                this.rating = value;
            }
        }

        public StatusFeedback Status
        {
            get
            {
                return this.status;
            }
            set
            {
                switch (value)
                {
                    case StatusFeedback.New:
                    case StatusFeedback.Unscheduled:
                    case StatusFeedback.Scheduled:
                    case StatusFeedback.Done:
                        this.status = value;
                        break;
                    default:
                        {
                            throw new ArgumentException("Feedback status must be New, Unscheduled, Scheduled or Done!");
                        }
                }
            }
        }

        public override StringBuilder GetWorkItemData()
        {
            const string newLineString = "\r\n";
            StringBuilder sb = new StringBuilder();
            sb.Append("WorkItem report need to be done");
            sb.Append(newLineString);

            return sb;
        }
    }
}
