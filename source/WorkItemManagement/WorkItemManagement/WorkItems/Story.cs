﻿using System;
using System.Text;
using WorkItemManagement.Histories;
using WorkItemManagement.Members;
using WorkItemManagement.WorkItems.Enums;

namespace WorkItemManagement.WorkItems
{
    public class Story : WorkItem, IStory
    {
        //Руси
        #region Fields
        private  PriorityTypes priority;
        private  SizeTypes size;
        private  StatusStory status;
        private  IMember assignee;
        #endregion
        #region Constructor
        public Story(string title, string description, PriorityTypes priority, SizeTypes size, StatusStory status, string assignee, string comment)
            : base(title, description, comment)
        {
            this.Priority = priority;
            this.Size = size;
            this.Status = status;
            this.Assignee = new Member(assignee, "To the data base");
            addHistory(new ActivityHistory($"{this.GetType().Name} with title" + title.ToString() + " is created!"));
        }
        #endregion
        #region Properties
        public PriorityTypes Priority
        {
            get
            {
                return this.priority;
            }
            set
            {
                switch (value)
                {
                    case PriorityTypes.High:
                        this.priority = value;
                        break;
                    case PriorityTypes.Medium:
                        this.priority = value;
                        break;
                    case PriorityTypes.Low:
                        this.priority = value;
                        break;
                    default:
                        {
                            throw new ArgumentException("Story priority must be \"High\", \"Medium\" or \"Low\"!");
                        }
                }
            }
        }
        public SizeTypes Size 
        {
            get
            {
                return this.size;
            }
            set
            {
                switch (value)
                {
                    case SizeTypes.Small:
                        this.size = value;
                        break;
                    case SizeTypes.Medium:
                        this.size = value;
                        break;
                    case SizeTypes.Large:
                        this.size = value;
                        break;
                    default:
                        {
                            throw new ArgumentException("Story size must be \"Small\", \"Medium\" or \"Large\"!");
                        }
                }

            }
        }
        public StatusStory Status
        {
            get
            {
                return this.status;
            }
            set
            {
                switch (value)
                {
                    case StatusStory.Done:
                        this.status = value;
                        break;
                    case StatusStory.InProgress:
                        this.status = value;
                        break;
                    case StatusStory.NotDone:
                        this.status = value;
                        break;
                    default:
                        //\"\"
                        {
                            throw new ArgumentException("Status of the story can be only \"Done\", \"In Progress\" or \"Not done\"!");
                        }
                }
            }
        }
        public IMember Assignee
        {
            get
            {
                return new Member(this.assignee.Name, this.assignee.WorkItemList, this.assignee.History);
            }

            set
            {
                if (value != null)
                {
                    this.assignee = new Member(value.Name, value.WorkItemList, value.History);
                }
                else
                {
                    throw new ArgumentException("Member object is null");
                }
            }
        }
        public override StringBuilder GetWorkItemData()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("WorkItem report need to be done");
            sb.AppendLine();
            return sb;
        }
        #endregion
        #region Methods
        #endregion

    }
}
