﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Histories;
using WorkItemManagement.Members;
using WorkItemManagement.WorkItems.Enums;

namespace WorkItemManagement.WorkItems
{
    public class Bug : WorkItem, IBug
    {
        //Васко
        private readonly IList<string> stepsToReproduce;
        private PriorityTypes priority;
        private SeverityTypes severity;
        private StatusBug status;
        private IMember assignee;

        public Bug(IList<string> stepsToReproduce, PriorityTypes priority, SeverityTypes severity, StatusBug status, IMember assignee, string title, string description, string comment)
            : base(title, description, comment)
        {
            this.stepsToReproduce = new List<string>();

            this.StepsToReproduce = stepsToReproduce;
            this.Priority = priority;
            this.Severity = severity;
            this.Status = status;
            this.Assignee = assignee;
            addHistory(new ActivityHistory($"{this.GetType().Name} with title" + title.ToString() + " is created!"));
        }

        public IList<string> StepsToReproduce
        {
            get
            {
                return new List<string>(this.stepsToReproduce);
            }

            set
            {
                if (null != value)
                {
                    foreach (var item in value)
                    {
                        AddStepToReproduce(item);
                    }
                }
                else
                {
                    throw new ArgumentNullException("The input list of \"Steps to reproduce\" is null");
                }
            }
        }

        public void AddStepToReproduce(string step)
        {
            if (null != step)
            {
                this.stepsToReproduce.Add(step);
            }
            else
            {
                throw new ArgumentNullException("The \"Step to reproduce\" should not be null");
            }
        }

        public PriorityTypes Priority
        {
            get
            {
                return this.priority;
            }

            set
            {
                switch (value) {
                    case PriorityTypes.High:
                    case PriorityTypes.Low:
                    case PriorityTypes.Medium:
                        this.priority = value;
                        break;
                    default:
                        {
                            throw new ArgumentOutOfRangeException("Bug priority must be High, Medium or Low!");
                        }
                }
            }
        }

        public SeverityTypes Severity
        {
            get
            {
                return this.severity;
            }

            set
            {
                switch (value)
                {
                    case SeverityTypes.Critical:
                    case SeverityTypes.Major:
                    case SeverityTypes.Minor:
                        this.severity = value;
                        break;
                    default:
                        {
                            throw new ArgumentOutOfRangeException("Bug severity must be Critical, Major or Minor!");
                        }
                }
            }
        }

        public StatusBug Status
        {
            get
            {
                return this.status;
            }

            set
            {
                switch (value)
                {
                    case StatusBug.Active:
                    case StatusBug.Fixed:
                        this.status = value;
                        break;
                    default:
                        {
                            throw new ArgumentOutOfRangeException("Bug status must be Active or Fixed!");
                        }
                }
            }
        }

        public IMember Assignee
        {
            get
            {
                return new Member(this.assignee.Name, this.assignee.WorkItemList, this.assignee.History);
            }

            set
            {
                if (null != value)
                {
                    this.assignee = new Member(value.Name, value.WorkItemList, value.History);
                }
                else
                {
                    throw new ArgumentNullException("Member object is null");
                }
            }
        }

        public override StringBuilder GetWorkItemData()
        {
            const string newLineString = "\r\n";
            StringBuilder sb = new StringBuilder();
            sb.Append("WorkItem report need to be done");
            sb.Append(newLineString);

            return sb;
        }
    }
}
