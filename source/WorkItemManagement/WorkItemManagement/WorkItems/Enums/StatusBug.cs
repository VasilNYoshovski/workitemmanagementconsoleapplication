﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkItemManagement.WorkItems.Enums
{
    public enum StatusBug
    {
        Active = 1,
        Fixed = 2
    }
}
