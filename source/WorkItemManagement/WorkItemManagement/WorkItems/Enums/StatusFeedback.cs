﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkItemManagement.WorkItems.Enums
{
    public enum StatusFeedback
    {
        New = 1,
        Unscheduled = 2,
        Scheduled = 3,
        Done = 4
    }
}
