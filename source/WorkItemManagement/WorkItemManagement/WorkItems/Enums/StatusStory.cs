﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkItemManagement.WorkItems.Enums
{
    public enum StatusStory
    {
        NotDone = 1,
        InProgress = 2,
        Done = 3
    }
}
