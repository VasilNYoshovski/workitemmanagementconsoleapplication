﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkItemManagement.WorkItems.Enums
{
    public enum PriorityTypes
    {
        High = 1,
        Medium = 2,
        Low = 3
    }
}
