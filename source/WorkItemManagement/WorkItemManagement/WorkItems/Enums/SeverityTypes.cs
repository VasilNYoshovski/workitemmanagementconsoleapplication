﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkItemManagement.WorkItems.Enums
{
    public enum SeverityTypes
    {
        Critical = 1, 
        Major = 2,
        Minor = 3
    }
}
