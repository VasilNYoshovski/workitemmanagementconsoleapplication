﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkItemManagement.WorkItems.Enums
{
    public enum SizeTypes
    {
        Large = 1,
        Medium = 2,
        Small = 3
    }
}
