﻿using WorkItemManagement.Members;
using WorkItemManagement.WorkItems.Abstract;
using WorkItemManagement.WorkItems.Enums;

namespace WorkItemManagement.WorkItems
{
    public interface IStory : IWorkItem
    {
        PriorityTypes Priority { set; get; }
        SizeTypes Size { set; get; }
        StatusStory Status { set; get; }
        IMember Assignee { set; get; }
    }
}
