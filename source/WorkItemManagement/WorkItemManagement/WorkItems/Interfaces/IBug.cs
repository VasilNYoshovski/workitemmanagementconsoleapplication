﻿using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Members;
using WorkItemManagement.WorkItems.Abstract;
using WorkItemManagement.WorkItems.Enums;

namespace WorkItemManagement.WorkItems
{
    public interface IBug : IWorkItem
    {
        IList<string> StepsToReproduce { set;  get; }
        PriorityTypes Priority { set; get; }
        SeverityTypes Severity { set; get; }
        StatusBug Status { set; get; }
        IMember Assignee { set; get; }

        void AddStepToReproduce(string step);
    }
}
