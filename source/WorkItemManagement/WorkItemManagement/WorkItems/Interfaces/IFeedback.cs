﻿using WorkItemManagement.WorkItems.Abstract;
using WorkItemManagement.WorkItems.Enums;

namespace WorkItemManagement.WorkItems
{
    public interface IFeedback : IWorkItem
    {
        int Rating { set; get; }
        StatusFeedback Status { set; get; }
    }
}
