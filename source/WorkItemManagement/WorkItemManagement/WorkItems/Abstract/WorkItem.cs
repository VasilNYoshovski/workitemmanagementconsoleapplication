﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Histories;
using WorkItemManagement.WorkItems.Abstract;

namespace WorkItemManagement.WorkItems
{
    public abstract class WorkItem : IWorkItem
    {
        private readonly string id;
        private string title;
        private string description;
        private readonly IList<string> commentsList;
        private readonly IList<IActivityHistory> historyList;

        public WorkItem(string title, string description, string comment)
        {
            this.id = GenerateUniqueID();
            this.title = title;
            this.description = description;
            this.commentsList = new List<string>();
            this.historyList = new List<IActivityHistory>();
            AddComments(comment);
        }

        private string GenerateUniqueID()
        {
            return Guid.NewGuid().ToString();
        }

        public string Title
        {
            get
            {
                return this.title;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("The title cannot be empty!");
                }
                else if (value.Length < 10 || value.Length > 50)
                {
                    throw new ArgumentException("The title's lenght must be between 10 and 50 symbols!");
                }
            }
        }

        public string Description
        {
            get
            {
                return this.description;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("The description cannot be empty!");
                }
                else if (value.Length < 10 || value.Length > 500)
                {
                    throw new ArgumentException("The description's lenght must be between 10 and 500 symbols!");
                }
            }
        }

        public string Id
        {
            get
            {
                return this.id;
            }
        }

        public IList<string> CommentsList
        {
            get
            {
                return new List<string>(this.commentsList);
            }
        }

        public IList<IActivityHistory> HistoryList
        {
            get
            {
                return (IList<IActivityHistory>) new List<IActivityHistory>(this.historyList);
            }
        }

        public void AddComments(string comment)
        {
            if (null != comment)
            {
                this.commentsList.Add(comment);
            }
            else
            {
                this.commentsList.Add("Empty Comment");
                //throw new ArgumentNullException("Comment is null");
            }
        }

        public void addHistory(IActivityHistory history)
        {
            if (null != history)
            {
                this.historyList.Add(history);
            }
            else
            {
                this.historyList.Add(new ActivityHistory("Empty Activity History"));
                //throw new ArgumentNullException("History is null");
            }
        }
        public abstract StringBuilder GetWorkItemData();
    }
}
