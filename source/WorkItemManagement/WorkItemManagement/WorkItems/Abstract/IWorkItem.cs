﻿using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Histories;

namespace WorkItemManagement.WorkItems.Abstract
{
    public interface IWorkItem
    {
        string Id { get; }
        string Title { get; }
        string Description { get; }

        IList<string> CommentsList { get; }
        IList<IActivityHistory> HistoryList { get; }

        StringBuilder GetWorkItemData();

        void AddComments(string comment);
        void addHistory(IActivityHistory history);
    }
}
