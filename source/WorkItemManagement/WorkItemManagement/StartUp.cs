﻿using Autofac;
using System;
using System.Linq;
using System.Reflection;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Providers;

namespace WorkItemManagement
{
    public class StartUp
    {
        static void Main()
        {
            var ass = Assembly.GetExecutingAssembly();
            var builder = new ContainerBuilder();
            builder.RegisterAssemblyTypes(ass).AsImplementedInterfaces();

            builder.RegisterType<InputProvider>().As<IInputProvider>();
            builder.RegisterType<OutputProvider>().As<IOutputProvider>();

            // Commands
            var commandTypes = ass.DefinedTypes.Where(typeInfo => typeInfo.ImplementedInterfaces.Contains(typeof(ICommanda))).ToList();
            foreach (var commandType in commandTypes)
            {
                builder.RegisterType(commandType.AsType()).Named<ICommanda>(commandType.Name.ToLower());
            }

            // Data
            builder.RegisterType<DataBase>().As<IDataBase>().SingleInstance();
            builder.RegisterType<WorkItemEngine>().As<IEngine>().SingleInstance();
            builder.RegisterType<SupportedServicesDataBase>().As<ISupportedServicesDataBase>().SingleInstance();

            // Engine
            var container = builder.Build();
            var engine = container.Resolve<IEngine>();
            engine.Start();
        }
    }
}
