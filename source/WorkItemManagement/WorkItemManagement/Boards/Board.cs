﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkItemManagement.Histories;
using WorkItemManagement.WorkItems;
using WorkItemManagement.WorkItems.Abstract;

namespace WorkItemManagement.Boards
{
    public class Board : IBoard
    {
        //Васко
        const int NAME_MIN_LENGHT = 5;
        const int NAME_MAX_LENGHT = 10;

        #region Fields
        private string name;
        private readonly IDictionary<string, IWorkItem> workItemList;
        private readonly IList<IActivityHistory> historyList;
        #endregion

        public Board(string boardName, string activityLocation)
        {
            this.workItemList = new Dictionary<string, IWorkItem>();
            this.historyList = new List<IActivityHistory>();

            this.Name = boardName;
            AddActivityHistory(new ActivityHistory($"Board with name {this.Name} has been added to {activityLocation}."));
        }

        public string Name
        {
            set
            {
                if (null != value)
                {
                    if ((NAME_MIN_LENGHT > value.Length) || (value.Length > NAME_MAX_LENGHT))
                    {
                        throw new ArgumentException("Board name must have lenght between 5 and 10 symbols.");
                    }
                    else
                    {
                        this.name = value;
                    }
                }
                else
                {
                    throw new ArgumentException("Board name must have lenght between 5 and 10 symbols.");
                }
            }

            get
            {
                return this.name;
            }
        }
        public IDictionary<string, IWorkItem> WorkItemList
        {
            get
            {
                return new Dictionary<string, IWorkItem>(this.workItemList);
            }
        }
        public void AddWorkItem(IWorkItem wi)
        {
            if (null != wi)
            {
                if (false == IsExistingWorkItem(wi.Id))
                {
                    this.workItemList.Add(wi.Id, wi);
                    AddActivityHistory(new ActivityHistory($"{wi.GetType().Name} {wi.Id} is added to {this.Name} board"));
                }
                else
                {
                    throw new ArgumentNullException($"{wi.GetType().Name} already exists in the dictionary");
                }
            }
            else
            {
                throw new ArgumentNullException($"WorkItem is null");
            }
        }
        public IList<IActivityHistory> HistoryList
        {
            get
            {
                return (IList<IActivityHistory>)this.historyList.AsEnumerable<IActivityHistory>();
            }
        }
        public void AddActivityHistory(IActivityHistory ah)
        {
            if (null != ah)
            {
                this.historyList.Add(ah);
            }
            else
            {
                throw new ArgumentNullException("HistoryList is null");
            }
        }
        public StringBuilder GetAllActivitiesOfBoard()
        {
            const string newLineString = "\r\n";
            StringBuilder sb = new StringBuilder();
            sb.Append($"All ActivityHistories of board {this.name} reports need to be done");
            sb.Append(newLineString);

            return sb;
        }

        public StringBuilder GetAllWorkItemsOfBoard()
        {
            const string newLineString = "\r\n";
            StringBuilder sb = new StringBuilder();
            sb.Append($"All WorkItems of board {this.name} reports need to be done");
            sb.Append(newLineString);

            return sb;
        }

        public bool IsExistingWorkItem(string wiID)
        {
            foreach (var item in this.workItemList)
            {
                if (wiID == item.Key)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
