﻿using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Histories;
using WorkItemManagement.WorkItems.Abstract;

namespace WorkItemManagement.Boards
{
    public interface IBoard
    {
        string Name { get; set; }
        IDictionary<string, IWorkItem> WorkItemList { get; }
        IList<IActivityHistory> HistoryList { get; }

        bool IsExistingWorkItem(string wiID);
        void AddWorkItem(IWorkItem wi);
        void AddActivityHistory(IActivityHistory ah);

        StringBuilder GetAllActivitiesOfBoard();
        StringBuilder GetAllWorkItemsOfBoard();
    }
}
