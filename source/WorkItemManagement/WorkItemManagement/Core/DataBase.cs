﻿using System;
using System.Collections.Generic;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Messages;
using WorkItemManagement.Members;
using WorkItemManagement.Teams;

namespace WorkItemManagement.Core
{
    public class DataBase : IDataBase
    {
        private const string NAME_OF_TEAM_CONTAINING_ALL_MEMBERS = " ";
        private readonly IDictionary<string, ITeam> teamsList;

        public DataBase()
        {
            this.teamsList = new Dictionary<string, ITeam>();
            this.teamsList.Add(NAME_OF_TEAM_CONTAINING_ALL_MEMBERS, new Team(NAME_OF_TEAM_CONTAINING_ALL_MEMBERS));
        }

        public string GetGlobalTeamName()
        {
            return NAME_OF_TEAM_CONTAINING_ALL_MEMBERS;
        }

        public ITeam GetGlobalTeam()
        {
            return teamsList[NAME_OF_TEAM_CONTAINING_ALL_MEMBERS];
        }

        public IDictionary<string, ITeam> TeamsList
        {
            get
            {
                return this.teamsList;
            }
        }

        public string AddNewTeamToTheTeamList(string teamName)
        {
            if (!IsRegisteredTeam(teamName))
            {
                if (!string.IsNullOrEmpty(teamName))
                {
                    this.teamsList.Add(teamName, new Team(teamName));
                    return string.Format(MessagesClass.TeamCreated, teamName);
                }
                else
                {
                    return string.Format(MessagesClass.InvalidTeamName, teamName);
                }
            }
            else
            {
                return string.Format(MessagesClass.TeamExists,teamName);
            }
        }
        public bool IsRegisteredTeam(string fteamName)
        {
            return this.teamsList.ContainsKey(fteamName);
        }

        public bool IsRegisteredMember(string fmemberName)
        {
                if (IsRegisteredTeam(NAME_OF_TEAM_CONTAINING_ALL_MEMBERS))
                {
                    return this.teamsList[NAME_OF_TEAM_CONTAINING_ALL_MEMBERS].IsExistingTeamMember(fmemberName);
                }
                return false;
        }

        public string RegisterMember(string memberName)
        {
            if (IsRegisteredTeam(NAME_OF_TEAM_CONTAINING_ALL_MEMBERS))
            {
                if (IsRegisteredMember(memberName))
                {
                    return $"Member with name {memberName} has already been registered.";
                }
                else
                {
                    try
                    {
                        this.teamsList[NAME_OF_TEAM_CONTAINING_ALL_MEMBERS].AddMemberToTeam(new Member(memberName, $"Member with name { memberName } has been registered"));
                    }
                    catch (ArgumentException ex)
                    {
                        return ex.Message;
                    }
                }
            }
            else
            {
                return $"Member with name {memberName} could not be created, because the global team does not exist";
            }
            return $"Member with name {memberName} has been registered.";
        }
    }
}
