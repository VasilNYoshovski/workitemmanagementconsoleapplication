﻿using Autofac.Core;
using Autofac.Core.Registration;
using WorkItemManagement.Core.Contracts;

namespace WorkItemManagement.Core
{
    public sealed class WorkItemEngine : IEngine
    {
        private readonly IInputProvider inputProvider;
        private readonly IOutputProvider outputProvider;
        private readonly ICommandFactory commandFactory;

        public WorkItemEngine(IInputProvider inputProvider,IOutputProvider outputProvider, ICommandFactory commandFactory)
        {
            this.inputProvider = inputProvider;
            this.outputProvider = outputProvider;
            this.commandFactory = commandFactory;
        }

        #region Methods
        public void Start()
        {
            do
            {
                outputProvider.PrintOutputLine("");
                outputProvider.PrintOutputLine($"Write \"Services\" or {(new SupportedServicesDataBase()).getCommandsNamesList().IndexOf("Services") + 1} or press ENTER to see the provided services.");
                outputProvider.PrintOutputLine($"Write \"ServicesContaining\" or {(new SupportedServicesDataBase()).getCommandsNamesList().IndexOf("ServicesContaining") + 1} to see filtered list of the provided services.");
                outputProvider.PrintOutputLine("Write \"exit\" or 0 to finish.");
                outputProvider.PrintOutput("Enter desired service or its number: ");

                var currentLine = inputProvider.InputLine().ToLower().Trim().Split();
                if (currentLine.Length == 1)
                {
                    var commandName = currentLine[0];
                    int cmdIndex;
                    if (int.TryParse(commandName, out cmdIndex))
                    {
                        var tmpSupportedCmds = new SupportedServicesDataBase().getCommandsNamesList();
                        if ((0 < cmdIndex) && (cmdIndex <= tmpSupportedCmds.Count))
                        {
                            commandName = tmpSupportedCmds[cmdIndex - 1];
                        }
                        else
                        {
                            if (cmdIndex == 0)
                            {
                                commandName = "exit";
                            }
                            else
                            {
                                outputProvider.PrintOutputLine($"Invalid command index: {commandName}");
                                continue;
                            }
                        }
                    }
                    if (commandName.Length == 0)
                    {
                        commandName = "Services";
                    }
                    if ("exit" == commandName.ToLower())
                    {
                        break;
                    }
                    try
                    {
                        var commanda = this.commandFactory.GetCommanda(commandName.ToLower());
                        this.outputProvider.PrintOutputLine();
                        this.outputProvider.PrintOutputLine($"--- [ {commandName} command selected ] ---");
                        this.outputProvider.PrintOutputLine(commanda.Execute(inputProvider, outputProvider));
                    }
                    catch (ComponentNotRegisteredException)
                    {
                        outputProvider.PrintOutputLine($"Unknown command: {commandName}");
                        //outputProvider.PrintOutputLine($"Unknown command: {commandName} :: {ex.Message}");
                        continue;
                    }
                    catch (DependencyResolutionException)
                    {
                        outputProvider.PrintOutputLine($"Unknown command");
                        //outputProvider.PrintOutputLine($"Unknown command: {ex.Message}");
                        continue;
                    }
                }
                else
                {
                    outputProvider.PrintOutputLine("Invalid command.");
                    continue;
                }
            } while (true);
        }
    }
    #endregion
}
