﻿namespace WorkItemManagement.Core.Messages
{
    public static class MessagesClass
    {
        public const string InvalidCommand = "INVALID COMMAND NAME: {0}!";
        public const string TeamExists = "Team with name {0} already exists!";
        public const string TeamDoesNotExists = "Team with name {0} was not registered up to now!";
        public const string TeamCreated = "Team with name {0} was created!";
        public const string MemberExists = "Member with name {0} already exists!";
        public const string MemberDoesNotExists = "Member with name {0} was not registered up to now!";
        public const string MemberCreated = "Member with name {0} was created!";
        public const string BoardIsCreated = "Board with name {0} was created!";
        public const string InvalidTeamName = "Invalid team name: {0}!";
        public const string InvalidTWorkItemName = "Invalid work item name: {0}!";
        public const string InvalidMemberName = "Invalid member name: {0}!";
        public const string AllTeamsShowed = "This is the list with names of all of the teams.";
        public const string NotExistingTeams = "There is no registred teams up to now!";
        public const string MemberWasAdded = "The member {0} was added to the team {1}!";
        public const string MemberAlreadyAdded = "The member {0} was already added to the team {1}!";
        public const string ThereIsNoMembers = "Currently there is no registered members.";
        public const string AllBoardsOfTeamShowed = "Currently those are all assigned to team {0} boards.";
        public const string AllPeopleCommand = "Currently all registred members are:\r\n";
        public const string AllMembersOfTeamShowed = "Currently those are all assigned to team {0} members.";
        public const string InvalidFeedbackName = "Invalid feedback name {0}!";
        public const string FeedbackExists = "Feedback with name {0} already exists!";
        public const string FeedbackDoesNotExists = "There is no registered feedback with name {0} in board with name {1}!";
        public const string FeedbackCreated = "Feedback was created!";
        public const string InvalidStatusType = "Invalid status type!";
        public const string StoryCreated = "Story with name {0} was created!";
        public const string InvalidPriorityType = "Invalid priority type!";
        public const string StatusChanged = "The status of feedback {0} was changed to {1}";
        public const string BoardActivitiesAreShown = "Summary for the activities for board with name {0}: {1}";
        public const string BoardBugCreated = "Bug with name {0} is created in {1} board";
        public const string BoardNotRegistred = "There is not registred board with name {0} is team with name {1}.";
        public const string MemberWasNotAdded = "Member {0} wasn`t added.";
        public const string FeedbackWasChanged = "The status of feedback {0} was changed to {1}!";
    }
}

