﻿using System.Text;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Messages;

namespace WorkItemManagement.Core.Services
{
    public class ShowAllTeams: ICommanda
    {
        private readonly IDataBase dataBase;

        public ShowAllTeams(IDataBase dataBase)
        {
            this.dataBase = dataBase;
        }
          
           public string Execute(IInputProvider ip, IOutputProvider op)
           {
                
                if (dataBase.TeamsList.Count < 2)
                {
                    return MessagesClass.NotExistingTeams;
                }
                else
                {
                StringBuilder sb = new StringBuilder();
                foreach (var item in dataBase.TeamsList)
                {
                    if (item.Key.ToString() == dataBase.GetGlobalTeamName())
                    {
                        continue;
                    }
                    else
                    {
                        sb.AppendLine(item.Key.ToString());
                    }
                }
                   op.PrintOutput(sb.ToString());
                    return MessagesClass.AllTeamsShowed;
                }
           }
    }
}
