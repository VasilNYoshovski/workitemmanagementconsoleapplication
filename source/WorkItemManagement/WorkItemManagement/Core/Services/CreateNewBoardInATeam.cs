﻿using System;
using WorkItemManagement.Boards;
using WorkItemManagement.Core.Contracts;

namespace WorkItemManagement.Core.Services
{
    public class CreateNewBoardInATeam : GetParametersClass, ICommanda
    {
        private readonly IDataBase dataBase;
        public CreateNewBoardInATeam(IDataBase dataBase)
        {
            this.dataBase = dataBase;
        }

        public string Execute(IInputProvider ip, IOutputProvider op)
        {
            var teamName = GetParamString("Team name", ip, op);
            try
            {
                if (false == dataBase.IsRegisteredTeam(teamName))
                {
                    throw new ArgumentException($"Team with name {teamName} does not exist");
                }
                var team = dataBase.TeamsList[teamName];
                var boardName = GetParamString("Board name", ip, op);
                Board board = new Board(
                    boardName,
                    team.Name);
                team.AddBoardToTeam(new Board(boardName, team.Name));
                return $"Board with name {boardName} was created in {team.Name} team!";
            }
            catch (ArgumentOutOfRangeException ex)
            {
                return ex.Message;
            }
            catch (ArgumentNullException ex)
            {
                return ex.Message;
            }
            catch (ArgumentException ex)
            {
                return ex.Message;
            }
        }
    }
}
