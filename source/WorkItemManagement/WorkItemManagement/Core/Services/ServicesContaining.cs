﻿using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Providers;

namespace WorkItemManagement.Core.Services
{
    public class ServicesContaining : GetParametersClass, ICommanda
    {
        public string Execute(IInputProvider ip, IOutputProvider op)
        {
            var filterKey = GetParamString("filter criteria", ip, op);
            var ssdb = new SupportedServicesDataBase();
            ssdb.PrintSupportedServicesListByFilter(filterKey, op);
            return "";
        }
    }
}
