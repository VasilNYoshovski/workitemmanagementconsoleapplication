﻿using System;
using System.Collections.Generic;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Members;
using WorkItemManagement.WorkItems;
using WorkItemManagement.WorkItems.Enums;

namespace WorkItemManagement.Core.Services
{
    public class CreateNewBugInBoard : GetParametersClass, ICommanda
    {
        private readonly IDataBase dataBase;
        public CreateNewBugInBoard(IDataBase dataBase)
        {
            this.dataBase = dataBase;
        }

        public string Execute(IInputProvider ip, IOutputProvider op)
        {
            var teamName = GetParamString("Team name", ip, op);
            try
            {
                if (false == dataBase.IsRegisteredTeam(teamName))
                {
                    throw new ArgumentException($"Team with name {teamName} does not exist");
                }
                var team = dataBase.TeamsList[teamName];
                var boardName = GetParamString("Board name", ip, op);
                if (false == team.IsExistingTeamBoard(boardName))
                {
                    throw new ArgumentException($"Board with name {boardName} does not exist in {team.Name} team");
                }
                var tmpBoard = team.BoardsDictionary[boardName];
                var memberName = GetParamString("Member name", ip, op);
                if (false == dataBase.IsRegisteredMember(memberName))
                {
                    throw new ArgumentException($"Member with name {memberName} does not exist in {team.Name} team");
                }
                try
                {
                    List<string> tmpList = new List<string>();
                    tmpList.Add(GetParamString("Steps to reproduce", ip, op));
                    PriorityTypes priority;
                    SeverityTypes severity;
                    StatusBug status;
                    if (false == Enum.TryParse(GetParamString("Priority", ip, op), out priority))
                    {
                        return "Invalid Priority value";
                    }
                    if (false == Enum.TryParse(GetParamString("Severity", ip, op), out severity))
                    {
                        return "Invalid Severity value";
                    }
                    if (false == Enum.TryParse(GetParamString("Status", ip, op), out status))
                    {
                        return "Invaldi Status value";
                    }
                    WorkItem wi = new Bug(
                        tmpList,
                        priority,
                        severity,
                        status,
                        new Member(memberName, teamName),
                        GetParamString("Title", ip, op),
                        GetParamString("Description", ip, op),
                        GetParamString("Comment", ip, op));
                    tmpBoard.AddWorkItem(wi);
                    return $"Bug with ID {wi.Id} was created in board with name {boardName} of {team.Name} team!";
                }
                catch (ArgumentException ex)
                {
                    return ex.Message;
                }
                catch (OverflowException ex)
                {
                    return ex.Message;
                }
            }
            catch (ArgumentException ex)
            {
                return ex.Message;
            }
        }
    }
}
