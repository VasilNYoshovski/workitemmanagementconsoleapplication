﻿using WorkItemManagement.Core.Contracts;

namespace WorkItemManagement.Core.Services
{
    class RegisterMember : GetParametersClass, ICommanda
    {
        private readonly IDataBase dataBase;
        public RegisterMember(IDataBase dataBase)
        {
            this.dataBase = dataBase;
        }
        public string Execute(IInputProvider ip, IOutputProvider op)
        {
            var memberName = GetParamString("Member name", ip, op);
            var message = dataBase.RegisterMember(memberName);
            return message;
        }
    }
}
