﻿using System;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.WorkItems;
using WorkItemManagement.WorkItems.Enums;

namespace WorkItemManagement.Core.Services
{
    class CreateNewStoryInBoard : GetParametersClass, ICommanda
    {
        private readonly IDataBase dataBase;
        public CreateNewStoryInBoard(IDataBase dataBase)
        {
            this.dataBase = dataBase;
        }

        public string Execute(IInputProvider ip, IOutputProvider op)
        {
            var teamName = GetParamString("Team name", ip, op);
            try
            {
                if (false == dataBase.IsRegisteredTeam(teamName))
                {
                    throw new ArgumentException($"Team with name {teamName} does not exist");
                }
                var team = dataBase.TeamsList[teamName];
                var boardName = GetParamString("Board name", ip, op);
                if (false == team.IsExistingTeamBoard(boardName))
                {
                    throw new ArgumentException($"Board with name {boardName} does not exist in {team.Name} team");
                }
                var tmpBoard = team.BoardsDictionary[boardName];
                var memberName = GetParamString("Member name", ip, op);
                if (false == dataBase.IsRegisteredMember(memberName))
                {
                    throw new ArgumentException($"Member with name {memberName} does not exist in {team.Name} team");
                }
                try
                {
                    PriorityTypes priority;
                    SizeTypes size;
                    StatusStory status;
                    if (false == Enum.TryParse(GetParamString("Priority", ip, op), out priority))
                    {
                        return "Invalid Priority value";
                    }
                    if (false == Enum.TryParse(GetParamString("Size", ip, op), out size))
                    {
                        return "Invalid Severity value";
                    }
                    if (false == Enum.TryParse(GetParamString("Status", ip, op), out status))
                    {
                        return "Invalid Status value";
                    }
                    var wi = new Story(
                        GetParamString("Title", ip, op),
                        GetParamString("Description", ip, op),
                        priority,
                        size,
                        status,
                        memberName,
                        GetParamString("Comment", ip, op));
                    tmpBoard.AddWorkItem(wi);
                    return $"Story with ID {wi.Id} was created in board with name {boardName} of {team.Name} team!";
                }
                catch (ArgumentException ex)
                {
                    return ex.Message;
                }
                catch (OverflowException ex)
                {
                    return ex.Message;
                }
            }
            catch (ArgumentException ex)
            {
                return ex.Message;
            }
        }
    }
}
