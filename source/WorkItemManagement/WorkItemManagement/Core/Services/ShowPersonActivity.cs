﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkItemManagement.Core.Contracts;

namespace WorkItemManagement.Core.Services
{
    public class ShowPersonActivity : GetParametersClass, ICommanda
    {
        private readonly IDataBase dataBase;
        public ShowPersonActivity(IDataBase dataBase)
        {
            this.dataBase = dataBase;
        }

        public string Execute(IInputProvider ip, IOutputProvider op)
        {
            var personName = GetParamString("Person name", ip, op);
            var lst = new List<string>();
            foreach (var teamItems in dataBase.TeamsList)
            {
                var members = teamItems.Value.MembersDictionary;
                var l = members.Where(personResult => personResult.Key == personName).FirstOrDefault();
                if (!(l.Value is null))
                {
                    foreach (var activityItem in l.Value.History)
                    {
                        {
                            lst.Add(activityItem.GetActivityData().ToString());
                        }
                    }
                }
            }
            if (0 == lst.Count)
            {
                return "found no results";
            }
            lst.Sort();
            op.PrintOutputLine($"{personName} has the following activities:");
            foreach (var item in lst)
            {
                op.PrintOutputLine(item);
            }
            return "";
        }
    }
}
