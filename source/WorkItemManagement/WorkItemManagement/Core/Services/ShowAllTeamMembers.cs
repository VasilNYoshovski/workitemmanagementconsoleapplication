﻿using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Messages;

namespace WorkItemManagement.Core.Services
{
    public class ShowAllTeamMembers : GetParametersClass, ICommanda
    {
        private readonly IDataBase dataBase;
        
        public ShowAllTeamMembers(IDataBase database)
        {
            this.dataBase = database;
        }


        public string Execute(IInputProvider ip, IOutputProvider op)
        {
            var teamName = GetParamString("Team name", ip, op);
            if (!string.IsNullOrEmpty(teamName))
            {
                if (dataBase.IsRegisteredTeam(teamName))
                {
                    string result = dataBase.TeamsList[teamName].GetAllMembers().ToString();
                    return result;
                        
                }
                else
                {
                    return string.Format(MessagesClass.TeamDoesNotExists, teamName);
                }
            }
            else
            {
                return string.Format(MessagesClass.InvalidTeamName, teamName);
            }
        }
    }
}
