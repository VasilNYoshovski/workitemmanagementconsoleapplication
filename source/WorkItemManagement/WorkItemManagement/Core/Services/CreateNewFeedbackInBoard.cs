﻿using System;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.WorkItems;
using WorkItemManagement.WorkItems.Enums;

namespace WorkItemManagement.Core.Services
{
    public class CreateNewFeedbackInBoard : GetParametersClass, ICommanda
    {
        private readonly IDataBase dataBase;
        public CreateNewFeedbackInBoard(IDataBase dataBase)
        {
            this.dataBase = dataBase;
        }

        public string Execute(IInputProvider ip, IOutputProvider op)
        {
            var teamName = GetParamString("Team name", ip, op);
            try
            {
                if (false == dataBase.IsRegisteredTeam(teamName))
                {
                    throw new ArgumentException($"Team with name {teamName} does not exist");
                }
                var team = dataBase.TeamsList[teamName];
                var boardName = GetParamString("Board name", ip, op);
                if (false == team.IsExistingTeamBoard(boardName))
                {
                    throw new ArgumentException($"Board with name {boardName} does not exist in {team.Name} team");
                }
                var tmpBoard = team.BoardsDictionary[boardName];
                try
                {
                    int rating;
                    if (false == int.TryParse(GetParamString("Rating", ip, op), out rating))
                    {
                        return "Invalid Rating value";
                    }
                    StatusFeedback status;
                    if (false == Enum.TryParse(GetParamString("Status", ip, op), out status))
                    {
                        return "Invalid Status value";
                    }
                    WorkItem wi = new Feedback(
                        GetParamString("Title", ip, op),
                        GetParamString("Description", ip, op),
                        status,
                        rating,
                        GetParamString("Comment", ip, op));
                    tmpBoard.AddWorkItem(wi);
                    return $"Feedback with ID {wi.Id} was created in board with name {boardName} of {team.Name} team!";
                }
                catch (ArgumentException ex)
                {
                    return ex.Message;
                }
                catch (OverflowException ex)
                {
                    return ex.Message;
                }
            }
            catch (ArgumentException ex)
            {
                return ex.Message;
            }
        }
    }
}
