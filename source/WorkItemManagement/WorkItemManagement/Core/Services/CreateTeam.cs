﻿using WorkItemManagement.Core.Contracts;

namespace WorkItemManagement.Core.Services
{
    public class CreateTeam : GetParametersClass , ICommanda
    {
        private readonly IDataBase dataBase;
        public CreateTeam(IDataBase dataBase)
        {
            this.dataBase = dataBase;
        }
       
        public string Execute(IInputProvider ip, IOutputProvider op)
        {
            var teamName = GetParamString("Team name", ip, op);

            var message = dataBase.AddNewTeamToTheTeamList(teamName);
            return message;

        }
               
    }
}
