﻿using WorkItemManagement.Core.Contracts;

namespace WorkItemManagement.Core.Services
{
    public abstract class GetParametersClass : IGetParametersClass
    {
        public string GetParamString(string userMessage, IInputProvider ip, IOutputProvider op)
        {
            int tmpCnt = 0;
            string tmpString;
            do
            {
                op.PrintOutput($"Enter {userMessage}: ");
                tmpString = ip.InputLine().Trim();
                if (string.IsNullOrEmpty(tmpString))
                {
                    tmpCnt++;
                    if (1 < tmpCnt)
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            } while (true);
            return tmpString;
        }
    }
}
