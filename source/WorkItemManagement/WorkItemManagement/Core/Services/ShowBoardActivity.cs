﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkItemManagement.Core.Contracts;

namespace WorkItemManagement.Core.Services
{
    public class ShowBoardActivity : GetParametersClass, ICommanda
    {
        private readonly IDataBase dataBase;
        public ShowBoardActivity(IDataBase dataBase)
        {
            this.dataBase = dataBase;
        }

        public string Execute(IInputProvider ip, IOutputProvider op)
        {
            var boardName = GetParamString("Board name", ip, op);
            List<string> lst = new List<string>();
            foreach (var teamItems in dataBase.TeamsList)
            {
                var boards = teamItems.Value.BoardsDictionary;
                var l = boards.Where(brdResult => brdResult.Key == boardName).FirstOrDefault();
                if (!(l.Value is null))
                {
                    foreach (var activityItem in l.Value.HistoryList)
                    {
                        {
                            lst.Add(activityItem.GetActivityData().ToString());
                        }
                    }
                }
            }
            if (0 == lst.Count)
            {
                return "found no results";
            }
            lst.Sort();
            op.PrintOutputLine($"{boardName} board has the following activities:");
            StringBuilder tmpSB = new StringBuilder();
            foreach (var item in lst)
            {
                tmpSB.AppendLine(item);
                //op.PrintOutputLine(item);
            }
            return tmpSB.ToString();
        }
    }
}
