﻿using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Messages;

namespace WorkItemManagement.Core.Services
{
    public class ShowAllPeople : ICommanda
    {
        private readonly IDataBase dataBase;
        
        public ShowAllPeople(IDataBase database)
        {
            this.dataBase = database;
        }
        public string Execute(IInputProvider ip, IOutputProvider op)
        {

            var team = this.dataBase.GetGlobalTeam();
            if (team.MembersDictionary.Count < 1)
            {
                return MessagesClass.ThereIsNoMembers;
            }
            else
            {
                var result = team.GetAllMembers().ToString();
                return result;
            }
        }
    }
}
