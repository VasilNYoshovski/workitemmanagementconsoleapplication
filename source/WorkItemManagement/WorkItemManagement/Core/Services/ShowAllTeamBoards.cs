﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Messages;

namespace WorkItemManagement.Core.Services
{
    public class ShowAllTeamBoards : GetParametersClass, ICommanda
    {
        private readonly IDataBase dataBase;

        public ShowAllTeamBoards(IDataBase database)
        {
            this.dataBase = database;
        }

        public string Execute(IInputProvider ip, IOutputProvider op)
        {
            var teamName = GetParamString("Team name", ip, op);

            if (!string.IsNullOrEmpty(teamName))
            {
                if (dataBase.IsRegisteredTeam(teamName))
                {
                    string result = dataBase.TeamsList[teamName].GetAllBoards().ToString();
                    
                    return result;

                }
                else
                {
                    return string.Format(MessagesClass.TeamDoesNotExists, teamName);
                }
            }
            else
            {
                return string.Format(MessagesClass.InvalidTeamName, teamName);
            }
        }
    }
}
