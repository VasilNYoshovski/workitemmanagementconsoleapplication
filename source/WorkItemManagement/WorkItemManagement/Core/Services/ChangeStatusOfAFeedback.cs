﻿using System;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Messages;
using WorkItemManagement.Teams;
using WorkItemManagement.WorkItems;
using WorkItemManagement.WorkItems.Enums;

namespace WorkItemManagement.Core.Services
{
    public class ChangeStatusOfAFeedback : GetParametersClass, ICommanda
    {
        private readonly IDataBase database;

        public ChangeStatusOfAFeedback(IDataBase database)
        {
            this.database = database;
        }
        public string Execute(IInputProvider ip, IOutputProvider op)
        {
            //StatusFeedback status;

            var teamName = GetParamString("Team name", ip, op);
            if (!database.IsRegisteredTeam(teamName))
            {
                return string.Format(MessagesClass.TeamDoesNotExists, teamName);
            }
            var boardName = GetParamString("Board name", ip, op);
            if (!database.TeamsList[teamName].IsExistingTeamBoard(boardName))
            {
                return string.Format(MessagesClass.BoardNotRegistred, boardName, teamName);
            }

            var feedbackName = GetParamString("Feedback name", ip, op);
            if (!database.TeamsList[teamName].BoardsDictionary[boardName].WorkItemList.ContainsKey(feedbackName))
            {
                return string.Format(MessagesClass.FeedbackDoesNotExists, feedbackName, boardName);
            }
            op.PrintOutput("Status of the Feedback can be only : ");
            op.PrintOutput("New");
            op.PrintOutput("Unscheduled");
            op.PrintOutput("Scheduled");
            op.PrintOutput("Done");
            var newStatus = GetParamString("Please enter the new status of the Feedback here: ", ip, op);
            
            if (Enum.TryParse(newStatus, out StatusFeedback status)== false)
            {
                return MessagesClass.InvalidStatusType;
            }
            
            var feedBack = database.TeamsList[teamName].BoardsDictionary[boardName].WorkItemList[feedbackName];
            var result = feedBack as Feedback;
            Enum.TryParse(newStatus, out StatusFeedback status2);
            var newStatusAsEnum = status2;
            result.Status = status2;
            return string.Format(MessagesClass.FeedbackWasChanged, feedbackName, status2);
        }
    }
}
