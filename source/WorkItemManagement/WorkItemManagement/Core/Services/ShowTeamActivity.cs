﻿using System;
using System.Text;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Messages;

namespace WorkItemManagement.Core.Services
{
    public class ShowTeamActivity : GetParametersClass, ICommanda
    {
        private readonly IDataBase dataBase;

        public ShowTeamActivity (IDataBase database)
        {
            this.dataBase = database;
        }

        public string Execute(IInputProvider ip, IOutputProvider op)
        {
            var teamName = GetParamString("Team name",ip, op);
            StringBuilder sb = new StringBuilder();
            if (!string.IsNullOrEmpty(teamName))
            {
                if (dataBase.IsRegisteredTeam(teamName))
                {
                    foreach (var item in dataBase.TeamsList[teamName].HistoryOfTeam)
                    {
                        sb.AppendLine($"On {item.ActivityDate.ToString()} was performed the following activities:");
                        foreach (var list in item.ActivityRecords)
                        {
                            sb.AppendLine(list);
                        }
                    }
                    op.PrintOutput(sb.ToString());
                    return "";
                }
                else
                {
                    return string.Format(MessagesClass.TeamDoesNotExists, teamName);
                }
            }
            else
            {
                return string.Format(MessagesClass.InvalidTeamName, teamName);
            }
        }
    }
}
