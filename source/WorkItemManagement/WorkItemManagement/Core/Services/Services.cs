﻿using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Providers;

namespace WorkItemManagement.Core.Services
{
    public class Services : GetParametersClass , ICommanda
    {
        public string Execute(IInputProvider ip, IOutputProvider op)
        {
            var ssdb = new SupportedServicesDataBase();
            ssdb.PrintSupportedServicesList(op);
            return "";
        }
    }
}
