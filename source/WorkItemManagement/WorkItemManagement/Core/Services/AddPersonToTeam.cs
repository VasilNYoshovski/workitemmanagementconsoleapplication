﻿using System;
using System.Linq;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Messages;
using WorkItemManagement.Histories;
using WorkItemManagement.Members;

namespace WorkItemManagement.Core.Services
{
    public class AddPersonToTeam : GetParametersClass, ICommanda
    {
        private readonly IDataBase dataBase;

        public AddPersonToTeam(IDataBase dataBase)
        {
            this.dataBase = dataBase;
        }
        public string Execute(IInputProvider ip, IOutputProvider op)
        {
            //var memberName = GetParamString("Member name", ip, op);
            //if (!string.IsNullOrEmpty(memberName))
            //{
            //    if (dataBase.IsRegisteredMember(memberName))
            //    {
            //        var teamName = GetParamString("Team name", ip, op);
            //        if (!string.IsNullOrEmpty(teamName))
            //        {
            //            if (dataBase.IsRegisteredTeam(teamName))
            //            {
            //                var currentTeam = dataBase.TeamsList[teamName];
            //                var member = dataBase.TeamsList[dataBase.NAME_OF_TEAM_CONTAINING_ALL_MEMBERS].
            //                    MembersDictionary[memberName];
            //                dataBase.TeamsList[teamName].MembersDictionary.Add(memberName, member);
            //                var history = new ActivityHistory(string.Format($"Member {memberName} was added to team {teamName}."));
            //                currentTeam.AddHistoryToTeamActivityHistory(history);
            //                member.AddActivityHistory(history);
            //                return string.Format(MessagesClass.MemberWasAdded, memberName, teamName);
                         
            //            }
            //            else
            //            {
            //                return string.Format(MessagesClass.TeamDoesNotExists, teamName);
            //            }
            //        }
            //        else
            //        {
            //            return string.Format(MessagesClass.InvalidTeamName, teamName);
            //        }
            //    }
            //    else
            //    {
            //        return string.Format(MessagesClass.MemberDoesNotExists, memberName);
            //    }
            //}
            //else
            //{
            //    return string.Format(MessagesClass.InvalidMemberName, memberName);
            //}
            //return string.Format(MessagesClass.MemberWasNotAdded, memberName);

            var teamName = GetParamString("Team name", ip, op);
            try
            {
                if (false == dataBase.IsRegisteredTeam(teamName))
                {
                    throw new ArgumentException($"Team with name {teamName} does not exist");
                }
                var team = dataBase.TeamsList[teamName];
                var memberName = GetParamString("Member name", ip, op);
                var board = new Member(
                    memberName,
                    team.Name);
                team.AddMemberToTeam(new Member(memberName, team.Name));
                return $"Member with name {memberName} was added to team {team.Name}!";
            }
            catch (ArgumentException ex)
            {
                return ex.Message;
            }
        }
    }
}
