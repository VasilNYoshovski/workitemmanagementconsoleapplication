﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using WorkItemManagement.Core.Contracts;

namespace WorkItemManagement.Core
{
    public class SupportedServicesDataBase : ISupportedServicesDataBase
    {
        public List<string> getCommandsNamesList()
        {
            var ass = Assembly.GetExecutingAssembly();
            var commandTypes = ass.DefinedTypes.Where(typeInfo => typeInfo.ImplementedInterfaces.Contains(typeof(ICommanda))).ToList();
            var commandsNamesList = new List<string>();
            foreach (var item in commandTypes)
            {
                commandsNamesList.Add(item.Name);
            }
            return commandsNamesList;
        }

        public void PrintSupportedServicesListByFilter(string content, IOutputProvider op)
        {
            List<string> l = getCommandsNamesList();
            bool commandsFound = false;
            int i = 0;
            op.PrintOutputLine("Supported services:");
            foreach (var item in l)
            {
                i++;
                if (item.ToLower().Contains(content.ToLower()))
                {
                    commandsFound = true;
                    op.PrintOutputLine($"( {i} ) : {item}");
                }
            }
            if (!commandsFound)
            {
                op.PrintOutputLine("None supported services found");
            }
        }

        public void PrintSupportedServicesList(IOutputProvider op)
        {
            List<string> l = getCommandsNamesList();
            int i = 0;
            op.PrintOutputLine("Supported services:");
            foreach (var item in l)
            {
                i++;
                op.PrintOutputLine($"( {i} ) : {item}");
            }
            if (0 == i)
            {
                op.PrintOutputLine("None supported services found");
            }
        }
    }
}
