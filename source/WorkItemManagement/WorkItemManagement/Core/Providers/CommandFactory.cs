﻿using Autofac;
using WorkItemManagement.Core.Contracts;

namespace WorkItemManagement.Core.Providers
{
    public class CommandFactory : ICommandFactory
    {
        private IComponentContext context;
        public CommandFactory(IComponentContext context)
        {
            this.context = context;
        }
        public ICommanda GetCommanda(string commandName)
        {
            return this.context.ResolveNamed<ICommanda>(commandName);
        }
    }
}
