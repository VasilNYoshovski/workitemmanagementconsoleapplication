﻿using System;
using System.Text;
using WorkItemManagement.Core.Contracts;

namespace WorkItemManagement.Core.Providers
{
    public class OutputProvider : IOutputProvider
    {
        public void PrintOutputLine()
        {
            Console.WriteLine();
        }

        public void PrintOutputLine(string text)
        {
            Console.WriteLine(text);
        }

        public void PrintOutput(string text)
        {
            Console.Write(text);
        }
    }
}
