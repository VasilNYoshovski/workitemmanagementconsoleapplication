﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Core.Contracts;

namespace WorkItemManagement.Core.Providers
{
    public class InputProvider : IInputProvider
    {
        public string InputLine()
        {
            return Console.ReadLine().Trim();
        }
    }
}
