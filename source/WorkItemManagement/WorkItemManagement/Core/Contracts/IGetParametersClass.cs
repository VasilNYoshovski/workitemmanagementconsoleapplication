﻿namespace WorkItemManagement.Core.Contracts
{
    public interface IGetParametersClass
    {
        string GetParamString(string userMessage, IInputProvider ip, IOutputProvider op);
    }
}
