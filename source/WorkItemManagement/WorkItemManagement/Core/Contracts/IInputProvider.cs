﻿namespace WorkItemManagement.Core.Contracts
{
    public interface IInputProvider
    {
        string InputLine();
    }
}
