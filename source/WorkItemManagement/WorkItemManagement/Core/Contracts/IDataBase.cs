﻿using System.Collections.Generic;
using WorkItemManagement.Teams;

namespace WorkItemManagement.Core.Contracts
{
    public interface IDataBase
    {
        string GetGlobalTeamName();
        ITeam GetGlobalTeam();
        IDictionary<string, ITeam> TeamsList { get; }
        bool IsRegisteredTeam(string fteamName);
        bool IsRegisteredMember(string fmemberName);
        string AddNewTeamToTheTeamList(string team);
        string RegisterMember(string memberName);
    }
}
