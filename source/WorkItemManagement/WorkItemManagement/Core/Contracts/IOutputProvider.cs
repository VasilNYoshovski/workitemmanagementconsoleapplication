﻿using System.Text;

namespace WorkItemManagement.Core.Contracts
{
    public interface IOutputProvider
    {
        void PrintOutput(string text);
        void PrintOutputLine();
        void PrintOutputLine(string text);
    }
}
