﻿namespace WorkItemManagement.Core.Contracts
{
    public interface ICommanda
    {
        string Execute(IInputProvider ip, IOutputProvider op);
    }
}
