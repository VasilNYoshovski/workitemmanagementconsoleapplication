﻿using System.Collections.Generic;
using System.Reflection;

namespace WorkItemManagement.Core.Contracts
{
    public interface ISupportedServicesDataBase
    {
        List<string> getCommandsNamesList();
        void PrintSupportedServicesListByFilter(string content, IOutputProvider op);
        void PrintSupportedServicesList(IOutputProvider op);
    }
}
