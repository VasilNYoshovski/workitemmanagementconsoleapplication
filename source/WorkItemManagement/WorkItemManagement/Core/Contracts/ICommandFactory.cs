﻿namespace WorkItemManagement.Core.Contracts
{
    public interface ICommandFactory
    {
        ICommanda GetCommanda(string commandName);
    }
}
