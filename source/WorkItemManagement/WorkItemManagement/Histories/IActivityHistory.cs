﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkItemManagement.Histories
{
    public interface IActivityHistory
    {
        DateTime ActivityDate { get; }
        IList<string> ActivityRecords { get; }
        void addActivityRecord(string description);

        StringBuilder GetActivityData();
    }
}
