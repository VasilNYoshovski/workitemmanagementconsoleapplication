﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace WorkItemManagement.Histories
{
    public class ActivityHistory : IActivityHistory
    {
        private readonly DateTime activityDate;
        private readonly IList<string> activityRecords;

        public ActivityHistory(string description)
        {
            this.activityDate = DateTime.Now;
            this.activityRecords = new List<string>();

            addActivityRecord(description);
        }

        public DateTime ActivityDate
        {
            get
            {
                return this.activityDate;
            }
        }

        public IList<string> ActivityRecords
        {
            get
            {
                return (IList<string>) activityRecords.AsEnumerable<string>();
            }
        }

        public void addActivityRecord(string description)
        {
            if (string.IsNullOrEmpty(description))
            {
                throw new ArgumentOutOfRangeException("Activity description should be atleast one character");
            }
            this.activityRecords.Add(description);
        }

        public StringBuilder GetActivityData()
        {
            var sb = new StringBuilder();
            sb.Append($"Activity at {this.activityDate}: ");
            foreach (var activityItem in activityRecords)
            {
                sb.AppendLine(activityItem);
            }
            return sb;
        }
    }
}
