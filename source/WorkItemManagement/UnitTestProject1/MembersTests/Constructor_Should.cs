﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections;
using System.Collections.Generic;
using WorkItemManagement.Histories;
using WorkItemManagement.Members;
using WorkItemManagement.WorkItems;

namespace UnitTestProject.MembersTests
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void AssignValues()
        {
            // Arrange
            string name = "Some member";
            var workItemListMock = new Mock<IDictionary<string, WorkItem>>();
            var historyListMock = new Mock<IList<ActivityHistory>>();
            string activityLocation = "";

            // Act
            var sut = new Member(name, activityLocation);

            // Assert
            Assert.AreEqual(name, sut.Name);
        }

        [TestMethod]
        public void InitializedDictionary_WorkItemList()
        {
            // Arrange, Act, Assert
            string name = "Some Member";
            string activityLocation = "";
            var sut = new Member(name, activityLocation);
            Assert.IsNotNull(sut.WorkItemList);
        }

        [TestMethod]
        public void InitializedDictionary_ActivityHistory()
        {
            // Arrange, Act, Assert
            string name = "Some Member";
            string activityLocation = "";
            var sut = new Member(name, activityLocation);
            Assert.IsNotNull(sut.History);
        }
    }
}
