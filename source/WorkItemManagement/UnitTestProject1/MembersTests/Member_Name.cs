﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Members;

namespace UnitTestProject.MembersTests
{
    [TestClass]
    public class Member_Name
    {
        [TestMethod]
        [DataRow(null)]
        [DataRow("adasdasdasdasdasdasdasdasdadadasdasdasdasdasdasdasdasdadadasdasdasdasdasdasdasdasdad")]
        [DataRow("a")]
        public void ThrowsWhenMemberNameIsInvalid(string invalidName)
        {
            // Arrange, Act, Assert
            var activityLocation = "";
            Assert.ThrowsException<ArgumentException>(() => new Member(invalidName, activityLocation));
        }

        [TestMethod]
        public void CourseNameIsValid()
        {
            // Arrange, Act, Assert
            try
            {
                var course = new Member("Some member", "");
            }
            catch (Exception ex)
            {
                if (ex.Message == "Member's name length should be between 5 and 15 symbols.")
                {
                    Assert.Fail();
                }
                else if (ex.Message == "The member's name cannot be empty!")
                {
                    Assert.Fail();
                }
            }
        }

        [TestMethod]
        public void CorrectlyAssignedName()
        {
            // Arrange
            string name = "Some member";
            string activityLocation = "";

            // Act
            var sut = new Member(name, activityLocation);

            // Assert
            Assert.AreEqual(name, sut.Name);
        }

        
    }
}
