﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using WorkItemManagement.Boards;
using WorkItemManagement.Histories;
using WorkItemManagement.Members;
using WorkItemManagement.Teams;
using WorkItemManagement.WorkItems;
using WorkItemManagement.WorkItems.Abstract;

namespace UnitTestProject.TeamsTests
{
    [TestClass]
    public class Should_Team
    {

        [TestMethod]
        [DataRow ("t1")]
        [DataRow("Teamt1")]
        [DataRow("1")]
        [DataRow("ABrandNewteam")]
        [DataRow("AveryveryveryveryveryveryveryveryveryveryveryveryveryverylongTeamname")]
        public void Should_ConstructorAssignCorrectValues(string name)
        {
            //Arrange
            var sut = new Team(name);
            //Act &  Assert
            Assert.AreEqual(sut.Name, name);
        }
        [TestMethod]
        [DataRow("t1")]
        [DataRow("Teamt1")]
        [DataRow("1")]
        [DataRow("ABrandNewteam")]
        [DataRow("AveryveryveryveryveryveryveryveryveryveryveryveryveryverylongTeamname")]
        public void Should_ConstructorCreateInstance(string name)
        {
            //Arrange
            var sut = new Team(name);
            //Act &  Assert
            Assert.IsInstanceOfType(sut , typeof(Team));
        }
        [TestMethod]
        [DataRow (null)]
        [DataRow ("")]
        public void Should_ThrowNullExeption(string name)
        {
            //Arrange, Act & Assert
            Assert.ThrowsException<ArgumentNullException>(() => new Team(name));
        }
        [TestMethod]
        [DataRow("t1")]
        [DataRow("Teamt1")]
        [DataRow("1")]
        [DataRow("ABrandNewteam")]
        [DataRow("AveryveryveryveryveryveryveryveryveryveryveryveryveryverylongTeamname")]
        public void Should_ConstructorCreateTeamDataBase(string name)
        {
            //Arrange
            var team = new Team(name);
            //Act & Assert
            Assert.IsNotNull(team.BoardsDictionary);
            Assert.IsNotNull(team.HistoryOfTeam);
            Assert.IsNotNull(team.MembersDictionary);
        }
        [TestMethod]
        public void Should_AddBoardToTeamBoardDictionary()
        {
            //Arrange
            var sut = new Team("t1");
         
            var fakeBoard = new Mock<Board>("board1", "location");
            var fakeBoard1 = new Mock<Board>("board2", "location1");
            var fakeBoard2 = new Mock<Board>("board3", "location2");
            //Act
            sut.AddBoardToTeam(fakeBoard.Object);
            sut.AddBoardToTeam(fakeBoard1.Object);
            sut.AddBoardToTeam(fakeBoard2.Object);
            //Assert
            Assert.AreEqual(sut.BoardsDictionary.Count, 3);
        }
        [TestMethod]
        public void Should_AddMemberToTeamMemberDictionary()
        {
            //Arrange
            var sut = new Team("t1");
            var fakeMember = new Mock<Member>("Pesho", "location");
            var fakeMember1 = new Mock<Member>("Pesho1", "location1");
            var fakeMember2 = new Mock<Member>("Pesho21", "location1");

            //Act
            sut.AddMemberToTeam(fakeMember.Object);
            sut.AddMemberToTeam(fakeMember1.Object);
            sut.AddMemberToTeam(fakeMember2.Object);

            //Assert
            Assert.AreEqual(sut.MembersDictionary.Count, 3);
        }
        [TestMethod]
        public void Should_AddActivityHistoryToTeamHistoryOfTeam()
        {
            //Arrange
            var sut = new Team("t1");

            var fakeHistory = new Mock<ActivityHistory>("description");
            var fakeHistory1 = new Mock<ActivityHistory>("description1");
            var fakeHistory2 = new Mock<ActivityHistory>("description2");
            var fakeHistory3 = new Mock<ActivityHistory>("description3");
            var fakeHistory4 = new Mock<ActivityHistory>("description4");

            //Act
            sut.AddHistoryToTeamActivityHistory(fakeHistory.Object);
            sut.AddHistoryToTeamActivityHistory(fakeHistory1.Object);
            sut.AddHistoryToTeamActivityHistory(fakeHistory2.Object);
            sut.AddHistoryToTeamActivityHistory(fakeHistory3.Object);
            sut.AddHistoryToTeamActivityHistory(fakeHistory4.Object);

            //Assert
            //Тук се подават с една бройка истории повече, защото при създаване на самия лист
            //с истории се генерира и първата - за самото създаване на Отбора.
            Assert.AreEqual(sut.HistoryOfTeam.Count, 6);
        }
        [TestMethod]
        public void Should_ThrowNullExceptionWhenTryToAddNullMemberValues()
        {
            //Arrange

            var team = new Team("team1");
            //Arrange Act & Assert
            Assert.ThrowsException<ArgumentNullException>(()=>team.AddMemberToTeam(null));
            Assert.ThrowsException<ArgumentNullException>(() => team.AddBoardToTeam(null));
            Assert.ThrowsException<ArgumentNullException>(() => team.AddHistoryToTeamActivityHistory(null));
        }
        [TestMethod]
        public void Should_CorrectlyAssignedInfoToDatabaseFields()
        {
            //Arrange
            var team = new Team("t1");

            var board = new Board("board1", "location");
            var board1 = new Board("board2", "location");
            var board2 = new Board("board3", "location");

            var history = new ActivityHistory("description");
            var history1 = new ActivityHistory("description1");
            var history2 = new ActivityHistory("description2");

            var member = new Member("member1", "location");
            var member1 = new Member("member2", "location");
            var member2 = new Member("member3", "location");
            //Act

            team.AddMemberToTeam(member);
            team.AddMemberToTeam(member1);
            team.AddMemberToTeam(member2);

            team.AddHistoryToTeamActivityHistory(history);
            team.AddHistoryToTeamActivityHistory(history1);
            team.AddHistoryToTeamActivityHistory(history2);

            team.AddBoardToTeam(board);
            team.AddBoardToTeam(board1);
            team.AddBoardToTeam(board2);
            
            Assert.AreEqual(team.HistoryOfTeam[1].ToString(), history.ToString());
            Assert.AreEqual(team.HistoryOfTeam[2].ToString(), history1.ToString());
            Assert.AreEqual(team.HistoryOfTeam[3].ToString(), history2.ToString());
            Assert.AreEqual(team.BoardsDictionary["board1"], board);
            Assert.AreEqual(team.BoardsDictionary["board2"], board1);
            Assert.AreEqual(team.BoardsDictionary["board3"], board2);
            Assert.AreEqual(team.MembersDictionary["member1"], member);
            Assert.AreEqual(team.MembersDictionary["member2"], member1);
            Assert.AreEqual(team.MembersDictionary["member3"], member2);

        }
        [TestMethod]
        public void Should_IsMemberExistsMethodWorkCorrect()
        {
            //Arrange
            var team = new Team("t1");
            var member = new Member("member1","location");
            //Act
            team.AddMemberToTeam(member);
            var boole = team.IsExistingTeamMember("member1");

            //Assert
            Assert.AreEqual(boole, true);
        }
        [TestMethod]
        public void Should_IsBoardExistsMethodWorkCorrect()
        {
            //Arrange
            var team = new Team("t1");
            var board = new Board("board1", "location");
            //Act
            team.AddBoardToTeam(board);
            var boole = team.IsExistingTeamBoard("board1");

            //Assert
            Assert.AreEqual(boole, true);
        }
 
    }
}
