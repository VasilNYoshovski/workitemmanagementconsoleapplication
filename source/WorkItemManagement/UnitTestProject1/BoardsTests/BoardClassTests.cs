﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using WorkItemManagement.Boards;
using WorkItemManagement.Histories;

namespace UnitTestProject.BoardsTests
{
    [TestClass]
    public class BoardClassTests
    {
        [TestMethod]
        [DataRow("BName", "Activiti History")]
        [DataRow("Valid Name", "Activiti History")]
        [DataRow("Board8", "Activiti History")]
        public void BoardConstructorCreatesInstanceSuccessfully(string boardName, string activityLocation)
        {
            // Arrange test
            Board sut;

            // Act test
            sut = new Board(boardName, activityLocation);

            // Assert test
            Assert.IsNotNull(sut);
        }

        [TestMethod]
        [DataRow("BName", "Activiti History")]
        [DataRow("Valid Name", "Activiti History")]
        [DataRow("Board8", "Activiti History")]
        public void BoardConstructorCreatesInstanceAndSetsTheNameProperlyWhenNameIsValid(string boardName, string activityLocation)
        {
            // Arrange test
            Board sut;

            // Act test
            sut = new Board(boardName, activityLocation);

            // Assert test
            Assert.IsNotNull(sut);
            Assert.AreEqual(boardName, sut.Name);
        }

        [TestMethod]
        [DataRow("", "Activiti History")]
        [DataRow("n", "Activiti History")]
        [DataRow("gh", "Activiti History")]
        [DataRow("age", "Activiti History")]
        [DataRow("Name", "Activiti History")]
        [DataRow("InvalidName", "Activiti History")]
        [DataRow("Very Invalid Name", "Activiti History")]
        public void BoardConstructorThrowsExceptionWhenNameIsInvalid(string boardName, string activityLocation)
        {
            // Arrange test

            // Act test & Assert test
            Assert.ThrowsException<ArgumentException>(() =>
            {
                new Board(boardName, activityLocation);
            });
        }

        [TestMethod]
        [DataRow("Activiti History")]
        public void BoardConstructorThrowsExceptionWhenNameIsNULL(string activityLocation)
        {
            // Arrange test
            string boardName = null;

            // Act test & Assert test
            Assert.ThrowsException<ArgumentException>(() =>
            {
                new Board(boardName, activityLocation);
            });
        }

        [TestMethod]
        [DataRow("BName", "Activiti History")]
        [DataRow("Valid Name", "Activiti History")]
        [DataRow("Board8", "Activiti History")]
        public void AddActivityHistoryAddsRecordSuccessfully(string boardName, string activityLocation)
        {
            // Arrange test
            var sut = new Board(boardName, activityLocation);
            var mockActivityHistory = new Mock<ActivityHistory>("hhh");

            // Act test
            sut.AddActivityHistory(mockActivityHistory.Object);

            // Assert test
            Assert.AreEqual(2, sut.HistoryList.Count);
        }

        [TestMethod]
        [DataRow("BName", "Activiti History")]
        [DataRow("Valid Name", "Activiti History")]
        [DataRow("Board8", "Activiti History")]
        public void AddActivityHistoryAddsSeveralSameRecordsSuccessfully(string boardName, string activityLocation)
        {
            // Arrange test
            var sut = new Board(boardName, activityLocation);
            var mockActivityHistory1 = new Mock<ActivityHistory>("r1");

            // Act test
            sut.AddActivityHistory(mockActivityHistory1.Object);
            sut.AddActivityHistory(mockActivityHistory1.Object);
            sut.AddActivityHistory(mockActivityHistory1.Object);

            // Assert test
            Assert.AreEqual(4, sut.HistoryList.Count);
        }

        [TestMethod]
        [DataRow("BName", "Activiti History")]
        [DataRow("Valid Name", "Activiti History")]
        [DataRow("Board8", "Activiti History")]
        public void AddActivityHistoryAddsSeveralDifferentRecordsSuccessfully(string boardName, string activityLocation)
        {
            // Arrange test
            var sut = new Board(boardName, activityLocation);
            var mockActivityHistory1 = new Mock<ActivityHistory>("r1");
            var mockActivityHistory2 = new Mock<ActivityHistory>("r2");
            var mockActivityHistory3 = new Mock<ActivityHistory>("r3");

            // Act test
            sut.AddActivityHistory(mockActivityHistory1.Object);
            sut.AddActivityHistory(mockActivityHistory2.Object);
            sut.AddActivityHistory(mockActivityHistory3.Object);

            // Assert test
            Assert.AreEqual(4, sut.HistoryList.Count);
        }

        [TestMethod]
        [DataRow("BName", "Activiti History")]
        [DataRow("Valid Name", "Activiti History")]
        [DataRow("Board8", "Activiti History")]
        public void AddActivityHistoryAddsRecordProperly(string boardName, string activityLocation)
        {
            // Arrange test
            var sut = new Board(boardName, activityLocation);
            var mockActivityHistory = new Mock<ActivityHistory>("hhh");

            // Act test
            sut.AddActivityHistory(mockActivityHistory.Object);

            // Assert test
            Assert.AreEqual(mockActivityHistory.Object, sut.HistoryList[1]);
        }

        [TestMethod]
        [DataRow("BName", "Activiti History")]
        [DataRow("Valid Name", "Activiti History")]
        [DataRow("Board8", "Activiti History")]
        public void AddActivityHistoryAddsSeveralSameRecordsProperly(string boardName, string activityLocation)
        {
            // Arrange test
            var sut = new Board(boardName, activityLocation);
            var mockActivityHistory1 = new Mock<ActivityHistory>("r1");

            // Act test
            sut.AddActivityHistory(mockActivityHistory1.Object);
            sut.AddActivityHistory(mockActivityHistory1.Object);
            sut.AddActivityHistory(mockActivityHistory1.Object);

            // Assert test
            Assert.AreEqual(mockActivityHistory1.Object, sut.HistoryList[1]);
            Assert.AreEqual(mockActivityHistory1.Object, sut.HistoryList[2]);
            Assert.AreEqual(mockActivityHistory1.Object, sut.HistoryList[3]);
        }

        [TestMethod]
        [DataRow("BName", "Activiti History")]
        [DataRow("Valid Name", "Activiti History")]
        [DataRow("Board8", "Activiti History")]
        public void AddActivityHistoryAddsSeveralDifferentRecordsProperly(string boardName, string activityLocation)
        {
            // Arrange test
            var sut = new Board(boardName, activityLocation);
            var mockActivityHistory1 = new Mock<ActivityHistory>("r1");
            var mockActivityHistory2 = new Mock<ActivityHistory>("r2");
            var mockActivityHistory3 = new Mock<ActivityHistory>("r3");

            // Act test
            sut.AddActivityHistory(mockActivityHistory1.Object);
            sut.AddActivityHistory(mockActivityHistory2.Object);
            sut.AddActivityHistory(mockActivityHistory3.Object);

            // Assert test
            Assert.AreEqual(mockActivityHistory1.Object, sut.HistoryList[1]);
            Assert.AreEqual(mockActivityHistory2.Object, sut.HistoryList[2]);
            Assert.AreEqual(mockActivityHistory3.Object, sut.HistoryList[3]);
        }

        [TestMethod]
        [DataRow("BName", "Activiti History")]
        [DataRow("Valid Name", "Activiti History")]
        [DataRow("Board8", "Activiti History")]
        public void AddActivityHistoryThrowsExceptionWhenReceivesNULL(string boardName, string activityLocation)
        {
            // Arrange test
            var sut = new Board(boardName, activityLocation);

            // Act test & Assert test
            Assert.ThrowsException<ArgumentNullException>(() =>
            {
                sut.AddActivityHistory(null);
            });
        }
    }
}
